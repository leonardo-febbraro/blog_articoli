<?php

namespace App\Models;

use App\Models\Magazine;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Article extends Model
{
    use HasFactory;

    protected $fillable=[
        "title",
        "description",
        "author",
        "img",
        "user_id"
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function magazines(){
        return $this->belongsToMany(Magazine::class);
    }
}
