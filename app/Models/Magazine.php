<?php

namespace App\Models;

use App\Models\Article;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Magazine extends Model
{
    use HasFactory;

    protected $fillable= [
        "title",
        "number",
        "price"
    ];

    public function articles(){
        return $this->belongsToMany(Article::class);
    }
}
