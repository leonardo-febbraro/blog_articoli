<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateRequest;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ArticleRequest;

class ArticleController extends Controller
{

    public function __construct()
    {
        $this->middleware("auth");
    }


    public function submit(ArticleRequest $req){
        // $article=new Article();
        // $article->title=$req->input("title");
        // $article->description=$req->input("description");
        // $article->author=$req->input("author");

        $article= Article::create([
            "title"=>$req->input("title"),
            "description"=>$req->input("description"),
            "author"=>Auth::user()->name,
            "img"=>$req->file("img")->store("public/img"),
            "user_id"=>Auth::id(),
        ]);

        if($req->magazine){

            $article->magazines()->attach($req->magazine);
        }


        return redirect(route("card"))->with("message", "Il tuo articolo è stato inserito");

    }

    public function details(Article $article){
        return view("details", compact("article"));
    }

    public function update(Article $article){
        return view("update", compact("article"));
    }

    public function edit(UpdateRequest $req, Article $article){
        $article->title=$req->title;
        $article->author=$req->author;
        $article->description=$req->description;
        if ($req->file("img")){

            $article->img=$req->file("img")->store("public/img");
        }
        $article->save();
        return redirect(route("details.articoli", compact("article")));
    }

    public function destroy(Article $article){
        if(count($article->magazines)>0){
            $article->magazines()->detach($article->magazine);
        }
        $article->delete();
        return redirect(route("card"));
    }

    public function user(){
        // $articles= Article::where("author", Auth::user()->name)->get();

        $articles= Auth::user()->articles;
        return view("userArticle", compact("articles"));
    }
}
