<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Contact;
use App\Models\Magazine;
use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;


class publicController extends Controller
{
    public function homepage(){
        return view("welcome");
    }

    

    public function articoli(){
        $magazines= Magazine::all();
        return view("articoli", compact("magazines"));
    }

    public function contatti(){
        return view("contatti");
    }

    public function submit(Request $request){

        $user= $request->input("user");
        $email= $request->input("email");
        $message= $request->input("description");
        $contact= compact("user","message");
        Contact::create([
            "user"=>$request->input("user"),
            "email"=>$request->input("email")
        ]);
        Mail::to($email)->send(new ContactMail($contact));
        return redirect(route("homepage"))->with("message", "Grazie per averci inviato un messaggio!");
    }

    

    public function card(){
        $articles=Article::orderBy("created_at", "DESC")->paginate(4);
        return view("card", compact("articles"));
    }
}
