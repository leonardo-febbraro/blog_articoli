<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title"=>"required|min:5",
            "description"=>"required",
            "img"=>"required|image"
        ];
    }

    public function messages()
{
    return [
        'title.required' => 'Titolo obbligatorio',
        'description.required' => 'Descrizione obbligatoria',
        "title.min"=>"Il titolo deve avere almeno 5 caratteri",
        "img.required"=>"Devi inserire 1 immagine"
    ];
}
}
