<?php

namespace Database\Seeders;

use Illuminate\Support\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */

    
    
    public function run()
    {
        $magazines=[
            ["title"=>"La Repubblica", "price"=>"1.20", "number"=>"24"],
            ["title"=>"Il Messagero", "price"=>"1.00", "number"=>"42"],
            ["title"=>"Il Fatto Quotidiano", "price"=>"1.50", "number"=>"65"],
            ["title"=>"Il Giornale", "price"=>"1.20", "number"=>"89"],
            ["title"=>"Il Foglio", "price"=>"1.10", "number"=>"69"],
        ];


        foreach ($magazines as $magazine) {
            # code...
            DB::table('magazines')->insert([
                'title' => $magazine["title"],
                'price' => $magazine["price"],
                'number' => $magazine["number"],
                "created_at"=> Carbon::now(),
                "updated_at"=> Carbon::now(),
            ]);
        }

        
    }
}
