<x-layout>
  @if (session('message'))
      <div class="alert alert-dark">
          {{ session('message') }}
      </div>
  @endif
  <header>
    <div  id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
          <div class="carousel-indicators">
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
          </div>
          <div class="carousel-inner">
            <div class="carousel-item active" style="background-image: url('https://cdn.pixabay.com/photo/2016/02/19/11/25/business-1209705_960_720.jpg')">
              <div class="carousel-caption">
                <h5>Lorem ipsum dolor sit amet consectetur adipisicing elit. </h5>
                <p>Consequatur quis sint doloribus illo voluptate quos culpa, repudiandae, consequuntur distinctio beatae dicta laudantium asperiores soluta obcaecati.</p>
              </div>
            </div>
            <div class="carousel-item" style="background-image: url('https://cdn.pixabay.com/photo/2015/05/31/10/55/man-791049_960_720.jpg')">
              <div class="carousel-caption">
                <h5>Lorem ipsum dolor sit amet consectetur adipisicing elit.</h5>
                <p>Voluptatibus nesciunt ipsum, facere vero quas quaerat sint iste accusamus.</p>
              </div>
            </div>
            <div class="carousel-item" style="background-image: url('https://cdn.pixabay.com/photo/2014/05/03/00/31/home-office-336581_960_720.jpg')">
              <div class="carousel-caption">
                <h5>Lorem ipsum dolor sit amet consectetur, adipisicing elit.</h5>
                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit.</p>
              </div>
            </div>
          </div>
          <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
          </button>
          <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
          </button>
        </div>
      </header>
      
      <!-- Page Content -->
      <section class="py-5">
        <div class="container">
          <h1 class="fw-light">Il miglior sito di informazione</h1>
          <p class="lead">Libero da ogni censura</p>
        </div>
      </section>

      {{-- seconda section --}}
      <section class="container-fluid">
        <div class="row">
          <div class="col-12 col-md-6 left-column">
            <img src="/img/news.png" alt="" class="img-fluid">
          </div>
          <div class="col-12 col-md-6 right-color">
            <img class="img-fluid" src="/img/file-transfer.png" alt="">
          </div>
        </div>
      </section>

      {{-- terza section --}}
      <div class="blog-post container-fluid my-5">
        <div class="blog-post__img">
          <img src="/img/jesuischarlie.jpg" alt="">
        </div>
        <div class="blog-post__info">
          <div class="blog-post__date">
            <span>Giovedì</span>
            <span>7 gennaio 2015</span>
          </div>
          <h2 class="blog-post__title">La strage di Charlie Hebdo con gli occhi dell’edicolante</h2>
          <p class="blog-post__text">
            Il 7 gennaio del 2015, a Parigi, due killer islamisti irrompono nella redazione del giornale satirico Charlie Hebdo e uccidono a colpi di mitra dodici persone inermi. Giornalisti, disegnatori, impiegati. Altri massacri di uguale matrice, e ugualmente ripugnanti, seguiranno in Francia: i più sanguinosi al teatro Bataclan e sul lungomare di Nizza. Ma quella mattanza rende esplicito anche ai ciechi e ai sordi l’irriducibile conflitto tra libertà d’espressione libertà tout court e fanatismo religioso. Nel fluire dei secoli, o prevarrà l’una oppure l’altro: la convivenza è impensabile.
            Ci siamo concessi il tempo, una volta seppelliti i morti, perfino di riflettere sulla liceità delle vignette “blasfeme” pagate con la vita. Fa parte, il beneficio del dibattito, dei tanti lussuosi privilegi di noi occidentali. Ma quel lusso — per fortuna — non abita nel libro di Anaïs Ginori
            L’edicolante di Charlie, che quella strage racconta con l’asciuttezza, direi con l’umiltà, del giornalismo “puro”, quello che considera suo dovere la testimonianza, il racconto, quella collazione dei fatti che, una volta composta, permette al cronista di ritrarsi affidando ad altri il giudizio, il comizio, la lite ideologica. 
          </p>
        </div>
      </div>
      <div class="blog-post container-fluid my-5">
        <div class="blog-post__img">
          <img src="/img/siberia-fiamme.jpg" alt="">
        </div>
        <div class="blog-post__info">
          <div class="blog-post__date">
            <span>Domenica</span>
            <span>2019</span>
          </div>
          <h2 class="blog-post__title">Incendio devasta la Siberia: sta per diventare il più grande rogo al mondo</h2>
          <p class="blog-post__text">
            Brucia anche la Siberia. Una delle zone note per essere fra le più fredde e inospitali del pianeta è diventato il teatro di uno degli incendi più grandi e devastanti della storia. A denunciaro è Greenpeace che lo pone come esempio tangibile degli sconvolgimenti climatici che stanno colpendo la Terra e che sono determinati dall'attività dell'uomo che, soprattutto a causa delle emissioni di CO2, sta determinando un rapido innalzamento della temperatura globale.
            L'incendio che infuria da settimane nel nordest della Siberia ha raggiunto un livello senza precedenti, con le fiamme che devastano un territorio pari a tutti gli altri incendi del mondo messi insieme. Lo denuncia Greenpeace Russia al Moscow Times, stimando che questo incendio potrebbe diventare il più grande nella storia documentata del pianeta.
            In Jacuzia, la regione più grande e più fredda della Russia, il fumo denso e acre copre gli insediamenti e raggiunge le città a migliaia di chilometri di distanza. Il più grande di questi incendi ha superato 1,5 milioni di ettari, ha detto il responsabile forestale del gruppo ambientalista.
            «Questo incendio deve crescere di circa 400.000 ettari per diventare il più grande nella storia documentata», ha detto Alexey Yaroshenko. «È impossibile contenere il fuoco attraverso gli sforzi umani: i vigili del fuoco dovrebbero spegnere le fiamme su una linea lunga 2.000 chilometri». Solo la pioggia potrebbe fermare o rallentare significativamente l'incendio, ha detto Yaroshenko, ma le precipitazioni attuali sono troppo deboli per farlo. «Nella migliore delle ipotesi potremmo salvare gli insediamenti e le infrastrutture che si trovano nel percorso del fuoco», ha spiegato.
            Gli esperti dicono che il rapido riscaldamento del clima della Jacuzia - la regione ha visto la sua temperatura media annuale aumentare di 3 gradi dall'inizio del XX secolo - combinato alla siccità record in 150 anni e a venti forti ha trasformato la sua vasta foresta di taiga in una polveriera. Le pratiche forestali dannose sono poi un fattore chiave dietro la diffusione senza precedenti degli incendi, ha detto l'esperta di Greenpeace Yulia Davydova, poiché le autorità regionali non sono tenute a spegnere gli incendi nelle cosiddette «zone di controllo», ovvero aree lontane dagli insediamenti umani. Il disboscamento, sia illegale che legale, è un'altra causa comune, secondo i nuovi dati acquisiti da Greenpeace.
          </p>
        </div>
      </div>
</x-layout>

