
    <x-layout>
        <!-- Page Content -->
    <div class="container-fluid">
    
        <!-- Page Heading -->
        <h1 class="my-4">Dettaglio articoli
        </h1>
    
        @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif
    
        <div class="row">
            
            
            <div>
                <img class="img-details" src="{{Storage::url($article->img)}}" alt="">
                <div class="col-12">
                    <div class="card-body">
                        <h5 class="card-title">
                            {{$article->author}}
                        </h5>
                        <h4 class="card-title">
                            {{$article->title}}
                        </h4>
                        <p class="card-text">{{$article->description}}</p>
                        @if (count($article->magazines) >=1)
                            @foreach ($article->magazines as $magazine)
                                
                            <p class="card-text">{{$magazine->title}}</p>
                            @endforeach
                        @endif
                        <p>{{$article->created_at->format("d/m/Y")}}</p>
                        <a href="{{route("card")}}" class="button-5">Torna indietro</a>
                        @auth
                        @if (Auth::user()->name===$article->author)
                            
                        <a href="{{route("details.update", compact("article"))}}" class="button-2 mx-3">Modificare</a>
                        <form method="POST" action="{{route("article.delete", compact("article"))}}">
                        @csrf
                        @method("delete")
                        <button class="button-3 my-2" type="submit">Elimina</button>
                        </form>
                        @endif  
                        @endauth
                    
                        
                    </div>
                </div>
            </div>
            
        </div>
    </x-layout>
