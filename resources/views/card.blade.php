<x-layout>
    <!-- Page Content -->
    <div class="container">
        
        <!-- Page Heading -->
        <h1 class="my-4">Articoli <a href="{{route("article.userArticle")}}" class="button-5">I miei articoli</a>
        </h1>
        
        @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
        @endif
        
        <div class="row">
            @foreach ($articles as $article)
            
            <div class="col-lg-6 mb-4">
                <div class="card h-100">
                    <img src="{{Storage::url($article->img)}}" alt="">
                    <div class="card-body">
                        <h5 class="card-title">
                            {{$article->author}}
                        </h5>
                        <h4 class="card-title">
                            {{$article->title}}
                        </h4>
                        <p class="card-text troncamento">{{$article->description}}</p>
                        <p>{{$article->created_at->format("d/m/Y")}}</p>
                        
                        <a href="{{route("details.articoli", compact("article"))}}" class="button-5">Vai al dettaglio</a>
                        
                    </div>
                </div>
            </div>
            @endforeach
            {{$articles->links()}}
        </div>
    </x-layout>