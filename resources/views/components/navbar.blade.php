<nav class="navbar navbar-expand-lg navbar-light">
  <div class="container-fluid">
    <i class="far fa-newspaper fa-3x"></i>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ms-auto">
        <li class="nav-item active">
          <a class="nav-link" href="{{Route("homepage")}}">Home</a>
        </li>
        @auth   
        <li class="nav-item active">
          <a class="nav-link" href="{{Route("articoli")}}">Articoli</a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="{{Route("card")}}">Card</a>
        </li>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="{{Route("contatti")}}">Contatti</a>
      </li>
      @endauth
      @guest
      <li class="nav-item active">
        <a class="nav-link" href="{{Route("card")}}">Card</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="{{Route("contatti")}}">Contatti</a>
      </li>
      
      <li class="nav-item active">
        <a class="nav-link" href="{{Route("login")}}"><i class="fas fa-sign-in-alt"></i> Login</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="{{Route("register")}}"><i class="fas fa-user-check"></i> Registrati</a>
      </li>
      @else
      <li class="nav-item dropstart">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
          Benvenuto/a, {{Auth::user()->name}}
        </a>
        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
          <li><a class="dropdown-item" href="{{ route('logout') }}"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
            Logout
          </a></li>
          
          <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
            @csrf
          </form>
        </ul>
      </li>
    </ul>
    @endguest
  </div>
</div>
</nav>