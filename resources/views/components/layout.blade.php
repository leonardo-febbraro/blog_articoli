<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">

    <title>Sporco.it</title>
  </head>
  <body
  @if (Route::currentRouteName()=="articoli")
      id="articoli"
      @elseif(Route::currentRouteName()=="contatti")
      id="contatti"
      @elseif(Route::currentRouteName()=="homepage")
      id="homepage"
  @endif
  >
    <x-navbar></x-navbar>

    

    {{$slot}}
        
    
        
    

    <x-footer></x-footer>
    <!-- Option 1: Bootstrap Bundle with Popper -->
    

    
    <script src="https://kit.fontawesome.com/76e5b8cfe9.js" crossorigin="anonymous"></script>
    <script src="{{asset('js/app.js')}}"></script>
  </body>
</html>