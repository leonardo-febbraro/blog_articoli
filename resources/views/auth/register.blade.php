<x-layout>
    <div class="container">
        <div class="row">
          <div class="col-lg-10 col-xl-9 mx-auto">
            <div class="card flex-row my-5 border-0 shadow rounded-3 overflow-hidden">
              <div class="card-img-left d-none d-md-flex">
                <!-- Background image for card set in CSS! -->
              </div>

              @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif


              <div class="card-body p-4 p-sm-5">
                <h5 class="card-title text-center mb-5 fw-light fs-5">Registrazione</h5>
                <form method="POST" action="{{route("register")}}">
                    @csrf
                  <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="floatingInputUsername" placeholder="myusername" required autofocus name="name">
                    <label for="floatingInputUsername">Nome Utente</label>
                  </div>
    
                  <div class="form-floating mb-3">
                    <input type="email" class="form-control" id="floatingInputEmail" placeholder="name@example.com" name="email">
                    <label for="floatingInputEmail">Email</label>
                  </div>
    
                  <hr>
    
                  <div class="form-floating mb-3">
                    <input type="password" class="form-control" id="floatingPassword" placeholder="Password" name="password">
                    <label for="floatingPassword">Password</label>
                  </div>
    
                  <div class="form-floating mb-3">
                    <input type="password" class="form-control" id="floatingPasswordConfirm" placeholder="Confirm Password" name="password_confirmation">
                    <label for="floatingPasswordConfirm">Conferma Password</label>
                  </div>
    
                  <div class="d-grid mb-2">
                    <button class="btn btn-lg btn-primary btn-login fw-bold text-uppercase" type="submit">Registrati</button>
                  </div>
    
                  <hr class="my-4">
    
                  <div class="d-grid mb-2">
                    <button class="btn btn-lg btn-google btn-login fw-bold text-uppercase" type="submit">
                      <i class="fab fa-google me-2"></i> Sign up with Google
                    </button>
                  </div>
    
                  <div class="d-grid">
                    <button class="btn btn-lg btn-facebook btn-login fw-bold text-uppercase" type="submit">
                      <i class="fab fa-facebook-f me-2"></i> Sign up with Facebook
                    </button>
                  </div>
    
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
</x-layout>