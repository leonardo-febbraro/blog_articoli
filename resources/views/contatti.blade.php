<x-layout>
    <div id="contatti" class="container-fluid px-5 my-5 fcon">
        <div class="row justify-content-center">
          <div class="col-xl-10">
            <div class="card border-0 rounded-3 shadow-lg overflow-hidden">
              <div class="card-body p-0">
                <div class="row g-0">
                  <div class="col-sm-6 d-none d-sm-block bg-image"></div>
                  <div class="col-sm-6 p-4">
                    <div class="text-center">
                      <div class="h3 fw-light">Form di contatto</div>
                      
                    </div>
      
                    <!-- * * * * * * * * * * * * * *
                // * * SB Forms Contact Form * *
                // * * * * * * * * * * * * * * *
      
                // This form is pre-integrated with SB Forms.
                // To make this form functional, sign up at
                // https://startbootstrap.com/solution/contact-forms
                // to get an API token!
                -->
      
                    <form  method="POST" action="{{Route("contacts.submit")}}">
                        @csrf
                      <!-- Name Input -->
                      <div class="form-floating mb-3">
                        <input class="form-control" id="name" type="text" placeholder="Name" data-sb-validations="required" name="user" />
                        <label for="name">Nome</label>
                        <div class="invalid-feedback" data-sb-feedback="name:required">Nome richiesto.</div>
                      </div>
      
                      <!-- Email Input -->
                      <div class="form-floating mb-3">
                        <input class="form-control" id="emailAddress" type="email" placeholder="Email Address" data-sb-validations="required,email" name="email" />
                        <label for="emailAddress">Email Address</label>
                        <div class="invalid-feedback" data-sb-feedback="emailAddress:required">Email richiesta.</div>
                        <div class="invalid-feedback" data-sb-feedback="emailAddress:email">Indirizzo email non valido.</div>
                      </div>
      
                      <!-- Message Input -->
                      <div class="form-floating mb-3">
                        <textarea class="form-control" id="message" type="text" placeholder="Message" style="height: 10rem;" data-sb-validations="required" name="description"></textarea>
                        <label for="message">Messaggio</label>
                        <div class="invalid-feedback" data-sb-feedback="message:required">Messaggio richiesto.</div>
                      </div>
                      <!-- Submit error message -->
                      <div class="d-none" id="submitErrorMessage">
                        <div class="text-center text-danger mb-3">Error sending message!</div>
                      </div>
      
                      <!-- Submit button -->
                      <div class="d-grid">
                        <button class="btn btn-primary btn-lg" id="submitButton" type="submit">Invia</button>
                      </div>
                    </form>
                    <!-- End of contact form -->
      
                  </div>
                </div>
      
              </div>
            </div>
          </div>
        </div>
      </div>
      
      <!-- CDN Link to SB Forms Scripts -->
      <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
</x-layout>