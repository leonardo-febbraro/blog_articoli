<x-layout>
    
    
    <div id="articoli" class="container-fluid my-5">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="card border-3 rounded-3 shadow-lg">
                    <div class="card-body p-4">
                        <div class="text-center">
                            <div class="h1 fw-light">Crea un articolo</div>
                        </div>
                        
                        
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        <!-- * * * * * * * * * * * * * *
                            // * * SB Forms Contact Form * *
                            // * * * * * * * * * * * * * * *
                            
                            // This form is pre-integrated with SB Forms.
                            // To make this form functional, sign up at
                            // https://startbootstrap.com/solution/contact-forms
                            // to get an API token!
                        -->
                        
                        <form method="POST" action="{{Route("article.submit")}}" enctype="multipart/form-data">
                            @csrf
                            
                            <!-- Name Input -->
                        
                            
                            <!-- Email Input -->
                            <div class="form-floating mb-3">
                                <input class="form-control" type="text" placeholder="Titolo" name="title"/>
                                <label for="emailAddress">Titolo</label>
                            </div>
                            <div class="mb-3">
                                <select name="magazine">
                                    <option value="">Seleziona una rivista</option>
                                    @foreach ($magazines as $magazine)
                                    
                                    <option value="{{$magazine->id}}">{{$magazine->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <!-- Message Input -->
                            <div class="form-floating mb-3">
                                <textarea class="form-control" type="text" name="description" placeholder="Message" style="height: 10rem;"></textarea>
                                {{old("description")}}
                                <label for="Message">Inserisci il tuo articolo</label>
                                
                            </div>
                            
                            <p>Inserisci un immagine</p>
                            <div class="mb-3">
                                <input type="file" name="img">
                            </div>

                            <!-- Submit button -->
                            <div class="d-grid">
                                <button class="btn btn-primary btn-lg" id="submitButton" type="submit">Invia</button>
                            </div>
                        </form>
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
</x-layout>