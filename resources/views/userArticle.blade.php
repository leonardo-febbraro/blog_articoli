<x-layout>
    <!-- Page Content -->
<div class="container">

    <!-- Page Heading -->
    <h1 class="my-4">Ecco i tuoi articoli {{Auth::user()->name}}
    </h1>
    

    <div class="row">
        @foreach ($articles as $article)
        
        <div class="col-lg-6 mb-4">
            <div class="card h-100">
                <img src="{{Storage::url($article->img)}}" alt="">
                <div class="card-body">
                    <h5 class="card-title">
                        {{$article->user->name}}
                    </h5>
                    <h4 class="card-title">
                        {{$article->title}}
                    </h4>
                    <p class="card-text">{{$article->description}}</p>
            
                        
                    <a href="{{route("details.articoli", compact("article"))}}" class="button-5">Vai al dettaglio</a>
                    
                </div>
            </div>
        </div>
        @endforeach
    </div>
</x-layout>