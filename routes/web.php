<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\ArticleController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[PublicController::class,"homepage"])->name("homepage");
Route::get('/articoli',[PublicController::class,"articoli"])->name("articoli");
Route::get('/contatti',[PublicController::class,"contatti"])->name("contatti");
Route::post("/contatti/submit", [PublicController::class, "submit"])->name("contacts.submit");
Route::get("/articoli/dettaglio", [PublicController::class,"dettaglio"])->name("dettaglio.articolo");
Route::post("/article/submit", [ArticleController::class, "submit"])->name("article.submit");
Route::get('/card',[PublicController::class,"card"])->name("card");
Route::get("/articoli/{article}", [ArticleController::class,"details"])->name("details.articoli");
Route::get("/articoli/update/{article}", [ArticleController::class,"update"])->name("details.update");
Route::put("/article/edit/{article}", [ArticleController::class, "edit"])->name("article.edit");
Route::delete("/article/destroy/{article}", [ArticleController::class, "destroy"])->name("article.delete");
Route::get("/article/user", [ArticleController::class, "user"])->name("article.userArticle");

